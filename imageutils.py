#!/usr/bin/env python3
import piexif
from datetime import datetime, timedelta

def _get_exif_value(tags, exif_tag):
    for sub_tags in tags.values():
        if type(sub_tags) == dict and exif_tag in sub_tags:
            return sub_tags[exif_tag].decode('utf8')

def GetImageDateTimeOriginal(filename):
    tags  = piexif.load(filename)
    tsStr = _get_exif_value(tags, piexif.ExifIFD.DateTimeOriginal)
    ts    = datetime.strptime(_get_exif_value(tags, piexif.ExifIFD.DateTimeOriginal), "%Y:%m:%d %H:%M:%S")

    return ts
