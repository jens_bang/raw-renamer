This is a Python script that will rename all Canon RAW files in the current directory based on the Creation date and time of the photo. The script will also rename any secondary files (darktable side car file, JPEG and so on with the same base name) to the same new base name.

## Prerequisites
* Python 3
* Standard Python libraries: glob, os, datetime
* piexif

## How to run it

1. Copy the 2 .py-files into a directory
2. (optional) Insert the directory in your path
3. Change to the directory where the RAW-files are
4. Run raw-renamer.py

## Disclaimers
This script is supplied as is, with absolutely no implied or specified warranty. It works for me, running on my Kubuntu 18.04, but I have no idea whether it will work for you on whatever system you're runningi. Not even if you're also running Kubuntu 18.04. So don't complain to me if it formats your gard drive or blows up your computer.

There are many things that need to be done, before this script is ready for wide distribution. First and foremost: I need to implement more error handling (any error handling, actually). And maybe implementing command line parameters, like the directory with the RAW files or the date/time format for the new file name.
