#!/usr/bin/env python3
import glob
import os
from datetime import datetime, timedelta

import imageutils

# Formats the file name string
def FormatFilename(base, i):
    return "{0}_{1:02d}".format(base, i)

# This function creates a filen name (without extension) by taking a string formatted from 
# the given timestamp and running through incrementing a counter until the corresponding 
# RAW file doesn't exist,
def MakeFilename(ts):
    newBase = datetime.strftime(ts, "%Y%m%d_%H%M%S")

    i = 0
    while (os.path.isfile("{0}.cr2".format(FormatFilename(newBase, i)))):
        i += 1

    return FormatFilename(newBase, i)

# Find all Canon RAW files in the current directory
files = sorted(glob.glob("*.CR2"))

for file in files:
    # Skip directories
    if (os.path.isfile(file)):
        filename, fileext = os.path.splitext(file)
    
        # This will of course find not only side car files, but also secondary image files (i.e. JPEGs), as well as the actual RAW file
        sidecars = sorted(glob.glob(filename + ".*"))
    
        ts = imageutils.GetImageDateTimeOriginal(file)                                
        newFilename = MakeFilename(ts)

        for sidecar in sidecars:
            # Avoiding directories
            if (os.path.isfile(sidecar)):
                filename, fileext = os.path.splitext(sidecar)
                # I prefer my file names and extensions in lower case
                fileext = fileext.lower()

                # Darktable side car files have 2 extensions, which both need to be retained
                if (fileext == ".xmp"):
                    fn2, fe2 = os.path.splitext(filename)
                    fileext = fe2.lower() + fileext

                destFilename = newFilename + fileext
                # A test for whether the rename went well should really be put in place. It would probably be best,
                # if I made the rename part into a recursive function. But we would then need to keep track of how far 
                # we got in the current rename cycle, i.e which files were renamed before the error occured? Besides it
                # wasn't realliy needed for my purpose. =D
                os.rename(sidecar, destFilename)
